import 'dart:io';
import 'dart:typed_data';

import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:flutter/material.dart';
import 'package:image/image.dart';
import 'package:pdf/pdf.dart';
import 'package:printing/printing.dart';
import 'package:screenshot/screenshot.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:esc_pos_utils/esc_pos_utils.dart';

class PrintPage extends StatefulWidget {
  const PrintPage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<PrintPage> createState() => _PrintPageState();
}

class _PrintPageState extends State<PrintPage> {
  final ScreenshotController screenshotController = ScreenshotController();
  String dir = Directory.current.path;
  final TextEditingController printerIp = TextEditingController();
  late Uint8List _imageFile;
  int port = 9100;



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Directionality(
      textDirection: TextDirection.rtl,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Screenshot(
              controller: screenshotController,
              child: Container(
                width: 450,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [
                        Text(
                          "نام: حامد عقیلی",
                          style: TextStyle(
                              fontWeight: FontWeight.w500, fontSize: 20),
                        ),
                        Text(
                          "شماره: 124566576768",
                          style: TextStyle(
                              fontWeight: FontWeight.w500, fontSize: 20),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: const [
                        Text(
                          "تلفن:09396289091",
                          style: TextStyle(
                              fontWeight: FontWeight.w500, fontSize: 20),
                        ),
                        Text(
                          "سفارش: 19:37 1401/12/18",
                          style: TextStyle(
                              fontWeight: FontWeight.w500, fontSize: 20),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: const [
                        Text(
                          "تحویل: تحویل در محل شعبه",
                          style: TextStyle(
                              fontWeight: FontWeight.w500, fontSize: 20),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Table(
                      defaultVerticalAlignment:
                          TableCellVerticalAlignment.middle,
                      columnWidths: const {
                        0: FlexColumnWidth(2),
                        1: FlexColumnWidth(1),
                        2: FlexColumnWidth(1),
                        3: FlexColumnWidth(1.2),
                      },
                      border: TableBorder.all(color: Colors.black),
                      children: const <TableRow>[
                        TableRow(children: [
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Padding(
                              padding: EdgeInsets.all(4.0),
                              child: Center(
                                child: Text(
                                  "شرح",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                ),
                              ),
                            ),
                          ),
                          TableCell(
                            child: Center(
                              child: Text(
                                "تعداد",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            ),
                          ),
                          TableCell(
                            child: Center(
                              child: Text(
                                "قیمت",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            ),
                          ),
                          TableCell(
                            child: Center(
                              child: Text(
                                "مبلغ کل",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            ),
                          ),
                        ]),
                        TableRow(children: [
                          TableCell(
                            child: Padding(
                              padding: EdgeInsets.all(5.0),
                              child: Center(
                                child: Text(
                                  "خورشت ماست تک نفره اقتصادی",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                ),
                              ),
                            ),
                          ),
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Center(
                                child: Text(
                              "4",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            )),
                          ),
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Center(
                                child: Text(
                              "10,000",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            )),
                          ),
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Center(
                              child: Text(
                                "40,000",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            ),
                          ),
                        ]),
                        TableRow(children: [
                          TableCell(
                            child: Padding(
                              padding: EdgeInsets.all(5.0),
                              child: Center(
                                child: Text(
                                  "چلو کباب کوبیده مخصوص سرآشپز با سیخ گوجه",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                ),
                              ),
                            ),
                          ),
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Center(
                                child: Text(
                              "2",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            )),
                          ),
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Center(
                                child: Text(
                              "80,000",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            )),
                          ),
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Center(
                              child: Text(
                                "160,000",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            ),
                          ),
                        ]),
                      ],
                    ),
                    Table(
                      columnWidths: const {
                        0: FlexColumnWidth(2),
                        1: FlexColumnWidth(1),
                      },
                      border: TableBorder.all(color: Colors.black),
                      children: const <TableRow>[
                        TableRow(children: [
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Padding(
                              padding:
                                  EdgeInsets.only(right: 4, bottom: 4, top: 4),
                              child: Text(
                                "جمع خرید کالاها",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            ),
                          ),
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Center(
                                child: Text(
                              "200,000",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            )),
                          ),
                        ]),
                        TableRow(children: [
                          TableCell(
                            child: Padding(
                              padding:
                                  EdgeInsets.only(right: 4, bottom: 4, top: 4),
                              child: Text(
                                "کد تخفیف",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            ),
                          ),
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Center(
                                child: Text(
                              "0",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            )),
                          ),
                        ]),
                        TableRow(children: [
                          TableCell(
                            child: Padding(
                              padding:
                                  EdgeInsets.only(right: 4, bottom: 4, top: 4),
                              child: Text(
                                "کرایه",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            ),
                          ),
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Center(
                              child: Text(
                                "12,000",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            ),
                          ),
                        ]),
                        TableRow(children: [
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Padding(
                              padding:
                                  EdgeInsets.only(right: 4, bottom: 4, top: 4),
                              child: Text(
                                "هزینه بسته بندی",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            ),
                          ),
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Center(
                                child: Text(
                              "7,000",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            )),
                          ),
                        ]),
                        TableRow(children: [
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Padding(
                              padding:
                                  EdgeInsets.only(right: 4, bottom: 4, top: 4),
                              child: Text(
                                "پرداختی از کیف پول",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            ),
                          ),
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Center(
                                child: Text(
                              "0",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20),
                            )),
                          ),
                        ]),
                        TableRow(children: [
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Padding(
                              padding:
                                  EdgeInsets.only(right: 4, bottom: 4, top: 4),
                              child: Text(
                                "پرداختی",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            ),
                          ),
                          TableCell(
                            verticalAlignment:
                                TableCellVerticalAlignment.middle,
                            child: Center(
                              child: Text(
                                "219,000",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20),
                              ),
                            ),
                          ),
                        ]),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Text(
                      "توضیحات: https://sarahpaz.org",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 100,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    onPrimary: Colors.indigo,
                    primary: Colors.indigo,
                    minimumSize: const Size(100, 60),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                  onPressed: () => printDirectDoc(context, printerIp.text),
                  child: const Text(
                    "پرینت مستقیم",
                    style: TextStyle(color: Colors.white, fontSize: 20.00),
                  ),
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    onPrimary: Colors.indigo,
                    primary: Colors.indigo,
                    minimumSize: const Size(100, 60),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                  onPressed: () => printDoc(context),
                  child: const Text(
                    "پرینت",
                    style: TextStyle(color: Colors.white, fontSize: 20.00),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    ));
  }

  Future<void> printDirectDoc(
      BuildContext context, final String printerIp) async {
    screenshotController.capture().then((Uint8List? image) async {
      await testPrint("192.168.100.6", image!);
    }).catchError((onError) {
      print(onError);
    });

/// کد های زیر مربوط به بحث پرینت کردن با پرینتر های معمولی سیستم می باشد و بر روی پرینتر های حرارتی کار نمی دهد

    ///print with pc printers
    // final profileImage = pw.MemoryImage(_imageFile);

    // final pdf = pw.Document();
    // pdf.addPage(pw.Page(
    //     pageFormat: PdfPageFormat.a5,
    //     build: (pw.Context context) {
    //       return pw.Center(child: pw.Image(profileImage)); // Center
    //     }));

    // List<Printer> pri = await Printing.listPrinters();
    // List<bool> defaultPrinters = [];
    // defaultPrinters = pri.map((e) => e.isDefault == true).toList();
    // await Printing.directPrintPdf(
    //     printer: Printer(
    //         isDefault: true,
    //         url: pri[defaultPrinters.indexOf(true)].url.toString()),
    //     onLayout: (PdfPageFormat format) async => pdf.save());
    // // print(pri!.url.toString());
    // // await Printing.directPrintPdf(
    // //     printer: pri!, onLayout: (PdfPageFormat format) async => doc.save());
  }

  // Future<void> scanPrinter() async {
  //   final stream = NetworkAnalyzer.discover2(
  //     '192.168.100.6',
  //     port,
  //     timeout: Duration(milliseconds: 5000),
  //   );
  //   int found = 0;
  //   stream.listen((NetworkAddress addr) {
  //     if (addr.exists) {
  //       found++;
  //       print('Found device: ${addr.ip}:$port');
  //     }
  //   }).onDone(() => print('Finish. Found $found device(s)'));
  // }

  Future<void> testPrint(String printerIp, Uint8List paperImage) async {
    PaperSize paper = PaperSize.mm80;

    final profile = await CapabilityProfile.load();
    final printer = NetworkPrinter(paper, profile);

    final PosPrintResult res = await printer.connect(printerIp, port: 9100);

    if (res == PosPrintResult.success) {
      await testReceipt(printer, paperImage);
      await Future.delayed(const Duration(seconds: 3), () {
        printer.disconnect();
      });
    }
  }

  Future<void> testReceipt(NetworkPrinter printer, Uint8List paperImage) async {
    final image = decodeImage(paperImage);
    printer.image(image!, align: PosAlign.center);
    printer.cut();
    printer.beep();
  }

  Future<void> printDoc(BuildContext context) async {
    screenshotController.capture().then((Uint8List? image) {
      setState(() {
        _imageFile = image!;
      });
    }).catchError((onError) {
      print(onError);
    });

    final profileImage = pw.MemoryImage(_imageFile);

    final pdf = pw.Document();
    pdf.addPage(pw.Page(
        pageFormat: PdfPageFormat.a5,
        build: (pw.Context context) {
          return pw.Center(child: pw.Image(profileImage)); // Center
        })); // Page

    await Printing.layoutPdf(onLayout: (_) => pdf.save());
  }
  //////////////////////////////
  // final image = await imageFromAssetBundle(
  //   "assets/images/image.png",
  // );
  // final doc = pw.Document();
  // doc.addPage(pw.Page(
  //     pageFormat: PdfPageFormat.a4,
  //     build: (pw.Context context) {
  //       return buildPrintableData(image);
  //     }));

  // List<Printer> pri = await Printing.listPrinters();
  // List<bool> defaultPrinters = [];
  // defaultPrinters = pri.map((e) => e.isDefault == true).toList();
  // await Printing.directPrintPdf(
  //     printer: Printer(
  //         isDefault: true,
  //         url: pri[defaultPrinters.indexOf(true)].url.toString()),
  //     onLayout: (PdfPageFormat format) async => doc.save());
  // // print(pri!.url.toString());
  // // await Printing.directPrintPdf(
  // //     printer: pri!, onLayout: (PdfPageFormat format) async => doc.save());
  ///////////////////////////////
}
