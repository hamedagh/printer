// import 'dart:io';
// import 'dart:typed_data';

// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:screenshot/screenshot.dart';
// import 'printable_data.dart';
// import 'package:printing/printing.dart';

// import 'package:pdf/pdf.dart';
// import 'package:pdf/widgets.dart' as pw;

// class SaveBtnBuilder extends StatefulWidget {
//   const SaveBtnBuilder({
//     Key? key,
//   }) : super(key: key);

//   @override
//   State<SaveBtnBuilder> createState() => _SaveBtnBuilderState();
// }

// class _SaveBtnBuilderState extends State<SaveBtnBuilder> {

//   @override
//   Widget build(BuildContext context) {
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//       children: [
//         ElevatedButton(
//           style: ElevatedButton.styleFrom(
//             onPrimary: Colors.indigo,
//             primary: Colors.indigo,
//             minimumSize: const Size(100, 60),
//             shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.circular(5),
//             ),
//           ),
//           onPressed: () => printDirectDoc(context),
//           child: const Text(
//             "پرینت مستقیم",
//             style: TextStyle(color: Colors.white, fontSize: 20.00),
//           ),
//         ),
//         ElevatedButton(
//           style: ElevatedButton.styleFrom(
//             onPrimary: Colors.indigo,
//             primary: Colors.indigo,
//             minimumSize: const Size(100, 60),
//             shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.circular(5),
//             ),
//           ),
//           onPressed: () => printDoc(context),
//           child: const Text(
//             "پرینت",
//             style: TextStyle(color: Colors.white, fontSize: 20.00),
//           ),
//         ),
//       ],
//     );
//   }


// }
